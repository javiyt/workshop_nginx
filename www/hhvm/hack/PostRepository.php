<?hh

class PostRepository
{

	public function getAllPosts()
	: Vector<Post> {
		return Vector{
			new Post(1, 'Post title 1', 'Post content 1'),
			new Post(2, 'Post title 2', 'Post content 2'),
			new Post(3, 'Post title 3', 'Post content 3'),
			new Post(4, 'Post title 4', 'Post content 4'),
			new Post(5, 'Post title 5', 'Post content 5')
		};
	}
}
