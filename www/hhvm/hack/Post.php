<?hh

class Post
{

	private int $likes_count;

	private Vector<Comment> $comments;

	public function __construct(
		private int $id,
		private string $title,
		private string $content
	)
	{
		$this->likes_count = 0;
		$this->comments = Vector{};
	}

	public function getId()
	: int {
		return $this->id;
	}

	public function getTitle()
	: string {
		return $this->title;
	}

	public function setTitle($new_title)
	: void {
		$this->title = $new_title;
	}

	public function getContent()
	: string {
		return $this->content;
	}

	public function setContent($new_content)
	: void {
		$this->content = $new_content;
	}

	public function getComments()
	: Vector<Comment> {
		return $this->comments;
	}

	public function addComment($new_comment)
	: void {
		$this->comments[] = $new_comment;
	}

	public function getLikesCount()
	: int {
		return $this->likes_count;
	}

	public function addLike()
	: void {
		$this->likes_count++;
	}
}
