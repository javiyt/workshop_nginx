<?hh

class Pager
{

	const ELEMENTS_PER_PAGE = 2;

	public function getPostsForPage(Vector<Post> $posts, int $page_number)
	{
		$offset = ($page_number - 1) * self::ELEMENTS_PER_PAGE;

		$sliced = array_slice($posts->toArray(), $offset, self::ELEMENTS_PER_PAGE);
		return new Vector($sliced);
	}
}
