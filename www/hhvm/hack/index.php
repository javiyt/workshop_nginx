<?hh

require 'Pager.php';
require 'Post.php';
require 'PostRepository.php';
require 'Comment.php';

function main()
: void {
	$posts_repository = new PostRepository();
	$all_posts = $posts_repository->getAllPosts();
	$pager = new Pager();
	$posts = $pager->getPostsForPage($all_posts, 1);

	printPosts($posts);
}

function printPosts($posts)
: void {
	if (empty($posts))
	{
		echo "No posts to display";
	}
	else
	{
		foreach( $posts as $post )
		{
			echo "<b>" . $post->getId() . ": " . $post->getTitle() . "</b><br>";
			echo $post->getContent() . "<br>";
			echo "<em>Likes count: " . $post->getLikesCount() . "</em><br>";
			echo "<br><hr>";
		}
	}
}

main();
