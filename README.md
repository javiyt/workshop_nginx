# Install nginx and basic configuration
sudo aptitude install nginx
[ http://localhost:8080/ ](http://localhost:8080/)

# Virtual host configuration
* Create a virtual host pointing to /vagrant/www
* Configure the vhost to serve only static files
* Disable default host
* Enable your host
* Reload nginx configuration

sudo rm /etc/nginx/sites-enabled/default

sudo ln -s /etc/nginx/sites-available/vagrant /etc/nginx/sites-enabled/vagrant

sudo /etc/init.d/nginx restart

# 404 custom page
* Create a 404 page under /vagrant/www
* Configure the vhost to serve the 404 page
* Reload nginx configuration

# Configuration by location
* Create a folder calle aliastest under /vagrant/www
* Create an index.html file inside
* Configure the vhost to redirect all calls to test folder to use the aliastest folder
* Reload nginx configuration

# Installing PHP-FPM
sudo aptitude install php5-fpm

# Configuring a pool
* Edit /etc/php5/fpm/pool.d/www.conf
* Set configuration for the pool:
** to run on port 9001
** to allow access only for localhost
** status can be checked at /fpm-status
** chroot to /vagrant/www
** set some environment variables

/etc/init.d/php5-fpm restart

mkdir /var/log/php-fpm/

# Configuring nginx to use PHP
* Edit vagrant host
* Add the location to connect to the pool when accessing a PHP file
[http://localhost:8080/info.php](http://localhost:8080/php/info.php)

# Configuring nginx to access the PHP-FPM status page
* Edit vagrant host
* Add the location to connect to the status page of the desired pool
[http://localhost:8080/fpm-status](http://localhost:8080/fpm-status)

# Configuring load balancing from nginx to PHP-FPM
* Create a pool identical to the one existing running on port 9002
* Create an environment variable to identify the pools
* Add [upstream](http://wiki.nginx.org/HttpUpstreamModule) configuration to access two different pools

# Installing HHVM
Follow the instructions you can find on [HHVM wiki page](https://github.com/facebook/hhvm/wiki/Prebuilt-packages-on-Ubuntu-14.04)

# Configuring HHVM
* Create a new folder hhvm in /vagrant/www
* Add a PHP file inside it
* Configure in your virtual host the access to that folder using the HHVM server

# Bonus image filter
* Configure a location to resize images on fly
* The URL should be something like (width)/(height)/(image file)
* Check [image filter documentation](http://nginx.org/en/docs/http/ngx_http_image_filter_module.html) for more information
* Image quality should be set to 75